# EKS Workers module



## Introduction

The module provisions the following resources:

- IAM Role and Instance Profile to allow Kubernetes nodes to access other AWS services
- Security Group with rules for EKS workers to allow networking traffic
- AutoScaling Group with Launch Template to configure and launch worker instances
- AutoScaling Policies and CloudWatch Metric Alarms to monitor CPU utilization on the EC2 instances and scale the number of instance in the AutoScaling Group up or down.
If you don't want to use the provided functionality, or want to provide your own policies, disable it by setting the variable `autoscaling_policies_enabled` to `"false"`.

## Usage

```hcl
provider "aws" {
  region = "${var.region}"
}

locals {
  # The usage of the specific kubernetes.io/cluster/* resource tags below are required
  # for EKS and Kubernetes to discover and manage networking resources
  # https://www.terraform.io/docs/providers/aws/guides/eks-getting-started.html#base-vpc-networking
  tags = "${merge(var.tags, map("kubernetes.io/cluster/${var.cluster_name}", "shared"))}"
}

data "aws_availability_zones" "available" {}

module "vpc" {
  source     = "git::https://gitlab.com/nutellino-playground/tf-modules/vpc.git?ref=0.0.1"
  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = "${var.attributes}"
  tags       = "${local.tags}"
  cidr_block = "10.0.0.0/16"
}

module "subnets" {
  source              = "git::https://gitlab.com/nutellino-playground/tf-modules/vpc-subnet.git?ref=0.0.1"
  availability_zones  = ["${data.aws_availability_zones.available.names}"]
  namespace           = "${var.namespace}"
  stage               = "${var.stage}"
  name                = "${var.name}"
  attributes          = "${var.attributes}"
  tags                = "${local.tags}"
  region              = "${var.region}"
  vpc_id              = "${module.vpc.vpc_id}"
  igw_id              = "${module.vpc.igw_id}"
  cidr_block          = "${module.vpc.vpc_cidr_block}"
  nat_gateway_enabled = "true"
}

module "eks_workers" {
  source                             = "git::https://gitlab.com/nutellino-playground/tf-modules/eks-workers.git?ref=0.0.1"
  namespace                          = "${var.namespace}"
  stage                              = "${var.stage}"
  name                               = "${var.name}"
  attributes                         = "${var.attributes}"
  tags                               = "${var.tags}"
  image_id                           = "${var.image_id}"
  eks_worker_ami_name_filter         = "${var.eks_worker_ami_name_filter}"
  instance_type                      = "${var.instance_type}"
  instance_type_secondary                  = "${var.instance_type_secondary}"
  on_demand_instance_count                 = "0"
  on_demand_percentage_above_base_capacity = "0"
  vpc_id                             = "${module.vpc.vpc_id}"
  subnet_ids                         = ["${module.subnets.public_subnet_ids}"]
  health_check_type                  = "${var.health_check_type}"
  min_size                           = "${var.min_size}"
  max_size                           = "${var.max_size}"
  wait_for_capacity_timeout          = "${var.wait_for_capacity_timeout}"
  associate_public_ip_address        = "${var.associate_public_ip_address}"
  cluster_name                       = "${var.cluster_name}"
  cluster_endpoint                   = "${var.cluster_endpoint}"
  cluster_certificate_authority_data = "${var.cluster_certificate_authority_data}"
  cluster_security_group_id          = "${var.cluster_security_group_id}"
  key_name                           = "nutellino"

  autoscaling_policies_enabled       = "false"
  bootstrap_extra_args               = "--kubelet-extra-args '--node-labels=cluster=prod,group=hot,node-role.kubernetes.io/prod-hot,node-role.kubernetes.io/spot-worker=true'"
  block_device_mappings = [{
    device_name = "/dev/xvda"

    ebs = [{
      volume_size           = "20"
      volume_type           = "gp2"
      delete_on_termination = true
    }]
  },
  ]
}
```




